"use strict";

/* let apiRequest = new Request("https://gitlab.com/api/v4/projects/22225666/repository/commits?per_page=50", {
  headers: new Headers({
    'PRIVATE-TOKEN': 'NPVLT2XUCiQYSvwyBDws'
  })
}); */

// Nutzung der API in CLI: curl --header "PRIVATE-TOKEN: NPVLT2XUCiQYSvwyBDws" "https://gitlab.com/api/v4/projects/22225666/repository/commits"

/* fetch(apiRequest)
  .then(response => response.json())
  .then(data => console.log(data)); */

 
    let apiRequest = new XMLHttpRequest("https://gitlab.com/api/v4/projects/22225666/repository/commits?per_page=50", {
      headers: new Headers({
        'PRIVATE-TOKEN': 'NPVLT2XUCiQYSvwyBDws'
      })});

    // Schritt 2: Event-Handler erstellen
    apiRequest.onload = function() {
        if(apiRequest.status == 200) {
        }
    };

    // Schritt 3:
    apiRequest.open("GET","https://gitlab.com/api/v4/projects/22225666/repository/commits?per_page=50");
    
    // Schritt 4:
    // wie sollen die Antwortdaten vorliegen?
    apiRequest.responseType = "json"; // geparstes JSON - alsoJS-Daten
    apiRequest.setRequestHeader("Accept","application/json");

    // Schritt 5:
    apiRequest.send();

    console.log(apiRequest);